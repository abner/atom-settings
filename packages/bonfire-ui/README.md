# Atom Dark UI theme

A dark theme incorporating warm and earthy colors. Intended for use with
[bonfire-syntax](https://github.com/circuitfox/bonfire-syntax).

![preview of the theme](https://raw.github.com/circuitfox/bonfire-ui/master/bonfire-preview.png?raw=true)
