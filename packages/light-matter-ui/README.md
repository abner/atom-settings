# Light Matter UI Theme

A beautiful light UI theme for GitHub's editor, [Atom](https://atom.io/).

![](https://raw.githubusercontent.com/chrishtanaka/light-matter-ui/master/screenshots/screenshot.png)

## Installation

#### Install w/ Atom

1. Navigate to `Atom -> Preferences`
2. Select the `themes` tab
3. Type `light matter` in the search box
4. Find `Light Matter UI` from the search results and click `Install`.

#### Install w/ Terminal
1. Make sure you have atom `shell commands` installed by navigating to `Atom -> Install Shell Commands`
2. Open a new terminal window
3. Type the command `apm install light-matter-ui`

#### Activating the Theme

Navigate to `Atom -> Preferences...`, select the `themes` tab, and select `Light Matter UI`, from the `UI Theme` dropdown.

## Additional Customization

#### Syntax Theme

[Light Matter Syntax](https://github.com/chrishtanaka/light-matter.git)
