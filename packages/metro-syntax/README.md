Metro
------

Blue based syntax highlighting theme meant to compliment the default color scheme of Windows 8.x

### Installation

* `apm install metro-syntax`

For the __complete experience__ you can also:

* `apm install metro-ui`

### Screenshots

Ruby:

![Ruby Screenshot](http://cirex.github.io/screenshots/metro-syntax.ruby.png "Ruby Screenshot")

__Note:__ The font in every screenshot is named __Consolas__ and it is included with __Windows__.
