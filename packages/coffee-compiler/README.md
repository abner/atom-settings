# Coffee Compiler

Quickly compile CoffeeScript code to JavaScript.

![Screenshot](https://raw.githubusercontent.com/forabi/coffee-compiler/master/screenshot.gif)

## Usage
1. Optionally select some text, if nothing is selected, the whole file will be compiled.
2. Run **Coffee Compiler: Compile** from the quick panel
3. That's it!

## Keybindings

- **cmd+shift+c** in OSX

- **ctrl+alt+c** in Windows/Linux
